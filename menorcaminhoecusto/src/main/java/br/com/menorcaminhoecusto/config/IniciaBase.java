package br.com.menorcaminhoecusto.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.menorcaminhoecusto.model.Rota;
import br.com.menorcaminhoecusto.repository.RotaRepository;

@Service
public class IniciaBase {
	
	@Autowired
	private RotaRepository repository;
	
	public void limparRotas() {
		try {
			repository.deleteAll();
			repository.flush();
		} catch (Exception e) {
			System.out.println("Erro ao Limpar Base");
		}
	}
	
	public void preencheBase() {
		try {
			Rota rota = new Rota();
			rota.setOrigem("A");
			rota.setDestino("B");
			rota.setDistancia(10.0);
			
			Rota rota1 = new Rota();
			rota1.setOrigem("B");
			rota1.setDestino("D");
			rota1.setDistancia(15.0);
			
			Rota rota2 = new Rota();
			rota2.setOrigem("A");
			rota2.setDestino("C");
			rota2.setDistancia(20.0);
			
			Rota rota3 = new Rota();
			rota3.setOrigem("C");
			rota3.setDestino("D");
			rota3.setDistancia(30.0);
			
			Rota rota4 = new Rota();
			rota4.setOrigem("B");
			rota4.setDestino("E");
			rota4.setDistancia(50.0);
			
			Rota rota5 = new Rota();
			rota5.setOrigem("D");
			rota5.setDestino("E");
			rota5.setDistancia(30.0);
			
			repository.saveAll(Arrays.asList(rota, rota1, rota2, rota3, rota4, rota5 ));
		} catch (Exception e) {
			System.out.println("Erro ao Preecher BD");
		}
	}
}
