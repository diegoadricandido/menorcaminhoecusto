package br.com.menorcaminhoecusto.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.menorcaminhoecusto.model.Rota;

@Service
public class BDMock {
	
	public List<Rota> obterRotas() {
		return iniciaRotas();
	}
	
	private List<Rota> iniciaRotas(){
		List<Rota> rotas = new ArrayList<Rota>();
		Rota rota = new Rota();
		rota.setOrigem("A");
		rota.setDestino("B");
		rota.setDistancia(10.0);
		rotas.add(rota);
		
		rota = new Rota();
		rota.setOrigem("B");
		rota.setDestino("D");
		rota.setDistancia(15.0);
		rotas.add(rota);
		
		rota = new Rota();
		rota.setOrigem("A");
		rota.setDestino("C");
		rota.setDistancia(20.0);
		rotas.add(rota);
		
		rota = new Rota();
		rota.setOrigem("C");
		rota.setDestino("D");
		rota.setDistancia(30.0);
		rotas.add(rota);
		
		rota = new Rota();
		rota.setOrigem("B");
		rota.setDestino("E");
		rota.setDistancia(50.0);
		rotas.add(rota);
		
		rota = new Rota();
		rota.setOrigem("D");
		rota.setDestino("E");
		rota.setDistancia(30.0);
		rotas.add(rota);
		
		return rotas;
	}
}
