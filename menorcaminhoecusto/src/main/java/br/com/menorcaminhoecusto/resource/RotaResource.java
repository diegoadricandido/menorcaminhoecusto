package br.com.menorcaminhoecusto.resource;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.menorcaminhoecusto.model.Rota;
import br.com.menorcaminhoecusto.model.dto.RotaDTO;
import br.com.menorcaminhoecusto.service.RotaService;

@RestController
@RequestMapping(value = "/rotas")
public class RotaResource {
	
	@Autowired
	private RotaService rotaService;
	
	@RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> inserir(@Valid @RequestBody RotaDTO rotaDTO) throws Exception {
		try {
			rotaService.adicionar(new Rota(rotaDTO));
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
    }
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<RotaDTO>> findAll(){
		List<Rota> rotas = rotaService.findAll();
		List<RotaDTO> rotasDTO = 
				rotas.stream().map(rota -> new RotaDTO(rota)).collect(Collectors.toList());
		return ResponseEntity.ok().body(rotasDTO);
	}
}