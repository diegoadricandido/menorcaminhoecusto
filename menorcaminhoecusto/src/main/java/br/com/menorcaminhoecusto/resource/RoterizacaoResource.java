package br.com.menorcaminhoecusto.resource;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.menorcaminhoecusto.model.DadosDestino;
import br.com.menorcaminhoecusto.model.MenorRota;
import br.com.menorcaminhoecusto.model.dto.EntradaDTO;
import br.com.menorcaminhoecusto.model.dto.MenorRotaDTO;
import br.com.menorcaminhoecusto.service.GerenciaRotasService;

@RestController
@RequestMapping(value = "/roterizacao")
public class RoterizacaoResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoterizacaoResource.class);

	@Autowired
	private GerenciaRotasService gerenciaRotasService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<MenorRotaDTO> verificar(@Valid @RequestBody EntradaDTO entradaDTO) {
		try {
			LOGGER.info("INICIANDO ROTERIZACAO");
			MenorRota menorRota = gerenciaRotasService.iniciaVerificacao(new DadosDestino(entradaDTO));
			LOGGER.info("FINALIZANDO ROTERIZACAO");
			return ResponseEntity.ok().body(menorRota != null ? new MenorRotaDTO(menorRota) : null);
		} catch (Exception e) {
			LOGGER.error("ERRO ROTERIZACAO: " + e.getMessage());
			return null;
		}
	}
}
