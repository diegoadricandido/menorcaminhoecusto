package br.com.menorcaminhoecusto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MenorcaminhoecustoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MenorcaminhoecustoApplication.class, args);
	}
}
