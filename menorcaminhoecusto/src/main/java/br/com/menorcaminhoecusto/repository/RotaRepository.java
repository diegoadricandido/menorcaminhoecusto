package br.com.menorcaminhoecusto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.menorcaminhoecusto.model.Rota;

@Repository
public interface RotaRepository extends JpaRepository<Rota, Long>{
	
}