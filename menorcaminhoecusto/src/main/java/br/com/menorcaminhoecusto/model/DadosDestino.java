package br.com.menorcaminhoecusto.model;

import java.io.Serializable;

import javax.persistence.Column;

import br.com.menorcaminhoecusto.model.dto.EntradaDTO;

public class DadosDestino implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(length = 1)
	private String origem;
	@Column(length = 1)
	private String destino;
	private Integer autonomia;
	private Double valorLitro;
	
	public DadosDestino() {
		// TODO Auto-generated constructor stub
	}
	
	public DadosDestino(EntradaDTO dto) {
		this.origem = dto.getOrigem();
		this.destino = dto.getDestino();
		this.autonomia = dto.getAutonomia();
		this.valorLitro = dto.getValorLitro();
	}
	
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public Integer getAutonomia() {
		return autonomia;
	}
	public void setAutonomia(Integer autonomia) {
		this.autonomia = autonomia;
	}
	public Double getValorLitro() {
		return valorLitro;
	}
	public void setValorLitro(Double valorLitro) {
		this.valorLitro = valorLitro;
	}
}
