package br.com.menorcaminhoecusto.model.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.menorcaminhoecusto.model.MenorRota;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MenorRotaDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("rota")
	private String rota;
	@JsonProperty("custo")
	private Double custo;
	
	public MenorRotaDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public MenorRotaDTO(MenorRota menorRota) {
		this.rota = menorRota.getRota();
		this.custo = menorRota.getCusto();
	}
	
	public String getRota() {
		return rota;
	}
	public void setRota(String rota) {
		this.rota = rota;
	}
	public Double getCusto() {
		return custo;
	}
	public void setCusto(Double custo) {
		this.custo = custo;
	}
}
