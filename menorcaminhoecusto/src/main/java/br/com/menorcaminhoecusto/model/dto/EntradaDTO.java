package br.com.menorcaminhoecusto.model.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EntradaDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	@JsonProperty("origem")
	private String origem;
	@JsonProperty("destino")
	private String destino;
	@JsonProperty("autonomia")
	private Integer autonomia;
	@JsonProperty("valorLitro")
	private Double valorLitro;
	
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public Integer getAutonomia() {
		return autonomia;
	}
	public void setAutonomia(Integer autonomia) {
		this.autonomia = autonomia;
	}
	public Double getValorLitro() {
		return valorLitro;
	}
	public void setValorLitro(Double valorLitro) {
		this.valorLitro = valorLitro;
	}
}
