package br.com.menorcaminhoecusto.model.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.menorcaminhoecusto.model.Rota;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RotaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "id")
	private Long id;
	@JsonProperty(value = "origem")
	private String origem;
	@JsonProperty(value = "destino")
	private String destino;
	@JsonProperty(value = "distancia")
	private Double distancia;
	
	public RotaDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public RotaDTO(Rota rota) {
		this.id = rota.getId() != null ? rota.getId() : null;
		this.origem = rota.getOrigem();
		this.destino = rota.getDestino();
		this.distancia = rota.getDistancia();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public Double getDistancia() {
		return distancia;
	}
	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}
}
