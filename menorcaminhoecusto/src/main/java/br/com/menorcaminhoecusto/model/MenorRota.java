package br.com.menorcaminhoecusto.model;

import java.io.Serializable;

public class MenorRota implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String rota;
	private Double custo;

	public String getRota() {
		return rota;
	}
	public void setRota(String rota) {
		this.rota = rota;
	}
	public Double getCusto() {
		return custo;
	}
	public void setCusto(Double custo) {
		this.custo = custo;
	}
}
