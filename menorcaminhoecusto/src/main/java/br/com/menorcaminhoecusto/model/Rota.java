package br.com.menorcaminhoecusto.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.com.menorcaminhoecusto.model.dto.RotaDTO;

@Entity(name = "ROTA")
public class Rota implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	@Column(name = "ORIGEM", length = 1)
	private String origem;
	@Column(name = "DESTINO", length = 1)
	private String destino;
	@Column(name = "DISTANCIA")
	private Double distancia;
	
	public Rota() {
		// TODO Auto-generated constructor stub
	}
	
	public Rota(RotaDTO dto) {
		this.id = dto.getId() != null ? dto.getId() : null;
		this.origem = dto.getOrigem();
		this.destino = dto.getDestino();
		this.distancia = dto.getDistancia();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public Double getDistancia() {
		return distancia;
	}
	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rota other = (Rota) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Rota [id=" + id + ", origem=" + origem + ", destino=" + destino + ", distancia=" + distancia + "]";
	}
	
}
