package br.com.menorcaminhoecusto.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.menorcaminhoecusto.model.DadosDestino;
import br.com.menorcaminhoecusto.model.MenorRota;
import br.com.menorcaminhoecusto.model.Rota;

@Service
public class GerenciaRotasService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GerenciaRotasService.class);

	@Autowired
	private RotaService rotaService;

	public MenorRota iniciaVerificacao(DadosDestino destino) throws Exception {
		try {
			List<Rota> rotas = rotaService.findAll();
			if (rotas.isEmpty()) {
				throw new Exception("Nao ha rotas disponiveis");
			}
			return trataRotaCrecente(rotas, destino);
		} catch (Exception e) {
			LOGGER.info("ERRO AO REALIZAR ROTERIZACAO: " + e.getLocalizedMessage());
			return null;
		}
	}

	private MenorRota trataRotaCrecente(List<Rota> rotas, DadosDestino destino) throws Exception {
		MenorRota menorRota = new MenorRota();
		List<Rota> encontrado = new ArrayList<Rota>();

		for (Rota rota : rotas) {
			if (rota.getOrigem().equals(destino.getOrigem()) && rota.getDestino().equals(destino.getDestino())) {
				if (!encontrado.isEmpty()) {
					encontrado.clear();
				}
				encontrado.add(rota);
				break;
			} else if (rota.getOrigem().equals(destino.getOrigem()) && !rota.getDestino().equals(destino.getDestino())) {
				encontrado.add(rota);
			} else if (!rota.getOrigem().equals(destino.getOrigem()) && rota.getDestino().equals(destino.getDestino())) {
				encontrado.add(rota);
			} else if (rota.getDestino().equals(destino.getDestino())) {
				encontrado.add(rota);
			}
		}
		preencherMenorRota(encontrado, menorRota, destino);
		validarRotaEncontrada(destino, menorRota);
		return menorRota;
	}

	private void validarRotaEncontrada(DadosDestino destino, MenorRota menorRota) throws Exception {
		if(!menorRota.getRota().substring(0).equals(destino.getOrigem())
				&& !menorRota.getRota().substring(menorRota.getRota().length() - 1).equals(destino.getDestino())) {
			throw new Exception("Rota nao encontrada");
		}
	}

	private void preencherMenorRota(List<Rota> rotas, MenorRota menorRota, DadosDestino destino) throws Exception {
		StringBuilder ponto = new StringBuilder();
		Double distancia = 0.0;
		ordenaCrescente(rotas);
		List<Rota> aux = new ArrayList<Rota>();
		
		for (int i = 0; i < rotas.size(); i++) {
		
			if (i == 0) {
				ponto.append(rotas.get(0).getOrigem());
				ponto.append(" -> ");
				ponto.append(rotas.get(0).getDestino());
				distancia += rotas.get(0).getDistancia();
				aux.add(rotas.get(i));
			} else if (i > 0) {
				if (rotas.get(i).getOrigem().equals(aux.get(aux.size() - 1).getDestino())) {
					if (rotas.get(i).getDestino().equals(destino.getDestino())) {
						ponto.append(" -> ");
						ponto.append(rotas.get(i).getDestino());
						distancia += rotas.get(i).getDistancia();
					}
				}
			}
		}
		menorRota.setRota(ponto.toString());
		menorRota.setCusto((distancia / destino.getAutonomia()) * destino.getValorLitro());
	}

	private void ordenaCrescente(List<Rota> rotas) {
		Collections.sort(rotas, new Comparator<Rota>() {
			@Override
			public int compare(Rota r1, Rota r2) {
				int cmpNome = r1.getOrigem().compareTo(r2.getOrigem());
	            if (cmpNome != 0)
	                return cmpNome;
	            else
	                return r1.getDestino().compareTo(r2.getDestino());
			}
		});
	}
}
