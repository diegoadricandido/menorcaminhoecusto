package br.com.menorcaminhoecusto.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.menorcaminhoecusto.model.Rota;
import br.com.menorcaminhoecusto.repository.RotaRepository;

@Service
public class RotaService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RotaService.class);
	@Autowired
	private RotaRepository rotaRepository;
	
	@Transactional
	public void adicionar(Rota rota) throws Exception {
		try {
			rotaRepository.save(rota);
		} catch (Exception e) {
			LOGGER.error("ERRO AO INSERIR ROTA: " + e.getMessage());
			throw new Exception("Erro ao Adicionar Rota");
		}
	}
	
	public List<Rota> findAll() {
		return rotaRepository.findAll();
	}
}
