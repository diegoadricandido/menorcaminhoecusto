package br.com.menorcaminhoecusto;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.menorcaminhoecusto.config.IniciaBase;
import br.com.menorcaminhoecusto.model.Rota;
import br.com.menorcaminhoecusto.service.RotaService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MenorcaminhoecustoApplicationTests {
	
	@Autowired
	private IniciaBase iniciaBase;
	@Autowired
	private RotaService rotaService;
	
	@Before
	public void iniciaDados() {
		iniciaBase.limparRotas();
		iniciaBase.preencheBase();
	}
	
	@Test
	public void testCarregarRotas() {
		List<Rota> rotas = rotaService.findAll();
		Assert.assertNotNull(rotas);
		Assert.assertEquals(6, rotas.size());
		Assert.assertEquals("D", rotas.get(0).getOrigem());
		Assert.assertEquals("E", rotas.get(0).getDestino());
	}
	
	@Test
	public void testInserirRota() throws Exception {
		Rota rota = new Rota();
		rota.setOrigem("A");
		rota.setDestino("B");
		rota.setDistancia(10.0);
		
		rotaService.adicionar(rota);
		
		List<Rota> rotas = rotaService.findAll();
		Assert.assertNotNull(rotas);
		Assert.assertEquals(7, rotas.size());
		Assert.assertEquals(rota.getOrigem(), rotas.get(6).getOrigem());
		Assert.assertEquals(rota.getDestino(), rotas.get(6).getDestino());
		Assert.assertEquals(rota.getDistancia(), rotas.get(6).getDistancia());
	}
}
