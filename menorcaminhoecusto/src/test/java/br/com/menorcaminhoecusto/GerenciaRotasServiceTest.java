package br.com.menorcaminhoecusto;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.menorcaminhoecusto.config.IniciaBase;
import br.com.menorcaminhoecusto.model.DadosDestino;
import br.com.menorcaminhoecusto.model.MenorRota;
import br.com.menorcaminhoecusto.service.GerenciaRotasService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GerenciaRotasServiceTest {

	@Autowired
	private IniciaBase iniciaBase;
	
	@Autowired
	private GerenciaRotasService gerenciaRotasService;
	
	@Before
	public void iniciaDados() {
		iniciaBase.limparRotas();
		iniciaBase.preencheBase();
	}
	
	@Test
	public void testRetornaRotaException() {
		try {
			iniciaBase.limparRotas();
			
			DadosDestino destino = new DadosDestino();
			destino.setOrigem("A");
			destino.setDestino("D");
			destino.setAutonomia(10);
			destino.setValorLitro(2.5);
			gerenciaRotasService.iniciaVerificacao(destino);
		} catch (Exception e) {
			Assert.assertEquals("Nao ha rotas disponiveis", e.getMessage());
		}
	}
	
	@Test
	public void testRetornaRotaComSucesso() throws Exception {
		iniciaBase.limparRotas();
		iniciaBase.preencheBase();
		
		DadosDestino destino = new DadosDestino();
		destino.setOrigem("A");
		destino.setDestino("D");
		destino.setAutonomia(10);
		destino.setValorLitro(2.5);
		Double custo = 6.25;
		MenorRota menorRota = gerenciaRotasService.iniciaVerificacao(destino);

		Assert.assertEquals("A -> B -> D", menorRota.getRota());
		Assert.assertEquals(custo, menorRota.getCusto());
	}
	
	@Test
	public void testRetornaRotaNaoEncontrada() {
		try {
			iniciaBase.limparRotas();
			iniciaBase.preencheBase();
			
			DadosDestino destino = new DadosDestino();
			destino.setOrigem("G");
			destino.setDestino("W");
			destino.setAutonomia(10);
			destino.setValorLitro(2.5);
			gerenciaRotasService.iniciaVerificacao(destino);
		} catch (Exception e) {
			Assert.assertEquals("Rota nao encontrada", e.getMessage());
		}
	}
}
