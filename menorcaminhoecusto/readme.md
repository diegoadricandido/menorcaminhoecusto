# Desafio - Malha Logística

## Descrição

Objetivo do projeto é encontrar a menor rota com o menor custo dentre as Rotas informadas.

## 1 - Configuração

### Banco de Dados

Banco de Dados MySql: 
    * Criar uma base com o nome `menorcaminhoecusto`;
    * No arquivo `application-dev.properties` (`/src/main/resources/application-dev.propertie`) estão as configurações de `Usuário` e `Senha` as mesmas deverão ser alteradas para a configuração informada no BD.
    * As Tabelas são criadas automaticamente toda vez que o aplicação é recarregado.

## 2 - Rodando a Aplicação
Para rodar a aplicação utilizar o comando Maven abaixo:
    * mvn spring-boot:run

## 3 - Consumindo a Aplicação

## 3.1 - Rotas
Para consumir os Serviços de Rotas utiliza-se os EndPoits abaixo:
    * Para adicionar uma `Nova Rota` Method: `POST` localhost:8080/rotas informando o JSON
      * `{  
         "origem": `String`,
         "destino": `String`,
         "distancia": `Double`
        }`
    * Para consultar as Rotas Method: `GET` localhost:8080/rotas, retornará o seguinte JSON
      * `[
          {
            "id": `Long`,
            "origem": `String`,
            "destino": `String`,
            "distancia": `Double`
          },
        ]`

## 3.2 - Roteirização
Para consumir os Serviços de Roteirização utiliza-se os EndPoits abaixo:
    * Para consultar uma `Roteirização` Method: `POST` localhost:8080/roteirizacao informando o JSON
      * `{  
         "origem": `String`,
         "destino": `String`,
         "autonomia": `Integer`,
         "valorLitro": `Double`
        }`
      
    * Retornará o Seguinte JSON:
    * `{
        "rota": `String`,
        "custo": `Double`
      }`
